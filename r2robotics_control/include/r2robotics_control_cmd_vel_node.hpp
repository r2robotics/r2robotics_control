#pragma once

#include <ros/ros.h>
#include <ros/console.h>
#include <std_msgs/Float64.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Trigger.h>

#include <cmath>

#include <r2robotics_msgs/Position.h>
#include <r2robotics_msgs/motors_speed_request.h>
#include <r2robotics_msgs/Movement.h>
#include <r2robotics_msgs/SonarArray.h>
#include <r2robotics_msgs/Velocity.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <gazebo_msgs/LinkStates.h>


#include "r2robotics_srvs/move_turrets_robot_frame.h"
#include "r2robotics_srvs/set_self_rotation.h"
#include "r2robotics_srvs/setControlMaxLinearSpeed.h"
#include "r2robotics_srvs/setControlMaxAngularSpeed.h"
#include "r2robotics_srvs/setControlMinLinearSpeed.h"
#include "r2robotics_srvs/setControlMinAngularSpeed.h"
#include "r2robotics_srvs/stopControl.h"
#include "r2robotics_srvs/resumeControl.h"
#include "r2robotics_srvs/startRecalibration.h"
#include "r2robotics_srvs/stopRecalibration.h"
#include "r2robotics_srvs/setControlTimeout.h"

#include "common.hpp"

using namespace control_common;

class Control
{
public:
    Control();

private:
    void odometryCallback(const r2robotics_msgs::Position::ConstPtr& feedback);
    void instructionCallback(const r2robotics_msgs::Movement::ConstPtr& instruction);
    void gazeboCallback(const gazebo_msgs::LinkStates::ConstPtr& feedback);
    void velocityCallback(const r2robotics_msgs::Velocity::ConstPtr& vel_instruction);

    void cmdVelCompute(PID& pid);
    void instructionCompute(std::vector<float>& pos_to_reach, Position2 rotation_center_position, double& yaw, int& direction);
    void cmdVelInstructionCompute(std::vector<float>& pos_to_reach, Position2 rotation_center_position, double& yaw, int& direction);

    void cmdVelPublish(PID& pid1, PID& pid2);
    void wheelsCmdPublish(std::vector<double> wheels_cmd, bool direction);

    void wheelsCmdConversion(PID& pid1, PID& pid2);

    bool setMaxLinearSpeed(r2robotics_srvs::setControlMaxLinearSpeed::Request &req,
                           r2robotics_srvs::setControlMaxLinearSpeed::Response &res);
    bool setMaxAngularSpeed(r2robotics_srvs::setControlMaxAngularSpeed::Request &req,
                            r2robotics_srvs::setControlMaxAngularSpeed::Response &res);
    bool setMinLinearSpeed(r2robotics_srvs::setControlMinLinearSpeed::Request &req,
                           r2robotics_srvs::setControlMinLinearSpeed::Response &res);
    bool setMinAngularSpeed(r2robotics_srvs::setControlMinAngularSpeed::Request &req,
                            r2robotics_srvs::setControlMinAngularSpeed::Response &res);
    bool stopControl(r2robotics_srvs::stopControl::Request &req,
                     r2robotics_srvs::stopControl::Response &res);
    bool resumeControl(r2robotics_srvs::resumeControl::Request &req,
                       r2robotics_srvs::resumeControl::Response &res);
    bool startRecalibration(r2robotics_srvs::startRecalibration::Request &req,
                    r2robotics_srvs::startRecalibration::Response &res);
    bool stopRecalibration(r2robotics_srvs::stopRecalibration::Request &req,
                            r2robotics_srvs::stopRecalibration::Response &res);
    bool setTimeout(r2robotics_srvs::setControlTimeout::Request &req,
                    r2robotics_srvs::setControlTimeout::Response &res);

    void incrementStop();
    void resetStop();
    void blockingDetection(std::vector<float> position);
    void blockingDetectionStraightLine(double distance);

    void setSubscribers();
    void setPublishers();
    void setClients();
    void setServices();

    void getParams();

    void timeout();

    void correctOrientation(double error, int index_left_wheel, int index_right_wheel, bool direction, std::vector<double> &commands);

    std_srvs::Trigger srv_waypoint{};
    r2robotics_srvs::set_self_rotation srv_rotation{};
    r2robotics_srvs::move_turrets_robot_frame srv_direction{};
    r2robotics_msgs::motors_speed_request pub;

    bool no_wp = false;
    bool omnidirectional = false;
    double wanted_direction = 0.0;

    ros::NodeHandle nh_;

    ros::ServiceClient client_waypoint;
    ros::ServiceClient client_steering;
    ros::ServiceClient client_rotation;

    ros::ServiceServer set_max_linear_speed;
    ros::ServiceServer set_max_angular_speed;
    ros::ServiceServer set_min_linear_speed;
    ros::ServiceServer set_min_angular_speed;
    ros::ServiceServer stop_control;
    ros::ServiceServer resume_control;
    ros::ServiceServer start_recalibration;
    ros::ServiceServer stop_recalibration;
    ros::ServiceServer set_timeout;

    ros::Publisher cmd_vel_;
    ros::Publisher cmd_wheels_;
    ros::Publisher blocking_detected_;
    ros::Publisher timeout_;

    ros::Subscriber gazebo_sub_;
    ros::Subscriber odometry_sub_;
    ros::Subscriber velocity_sub_;
    ros::Subscriber instruction_sub_;

    PID pid_cmd_vel_linear;
    PID pid_cmd_vel_angular;

    PID pid_distance;
    PID pid_orientation;

    Mode mode;
    Mode previous_mode;
    Movement movement_mode;

    double max_speed;

    bool potential = true;
    bool global = false;
    bool simulation_active;

    std::vector<Wheel> wheels;
    std::vector<float> wheels_speeds;
    float correction_coefficient;

    double tn_secs;
    double tn_1_secs;
    double time_beginning_movement = 0.;
    double timeout_duration = 10.;

    bool waypoint_reached = false;

    int time_stop = 0;
    int time_not_moving = 0;
    double yaw_error_ref = 0.;

    bool emergency_stop = false;

    double robot_direction = 1.0;

    Pose2 current_pose{0., 0., 0.};
    Pose2 initial_pose{0., 0., 0.};
    Position2 instant_rotation_center{0., 0.};

    geometry_msgs::Twist cmd_vel;

};
