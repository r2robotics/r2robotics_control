#include "r2robotics_control_cmd_vel_node.hpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "control");
    Control control{};

    ros::spin();
}
