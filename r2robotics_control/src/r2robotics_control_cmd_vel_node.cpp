#include "r2robotics_control_cmd_vel_node.hpp"
#include "colors.h"

using namespace std;
using namespace control_common;

Control::Control()
{
    setSubscribers();
    setPublishers();
    setClients();
    setServices();

    getParams();

    ROS_CONTROL_STREAM("Parameters loaded: ");
    ROS_CONTROL_STREAM("Omnidirectional set to " << omnidirectional);
    ROS_CONTROL_STREAM("Number of wheels " << wheels.size());
    ROS_CONTROL_STREAM("Coefficients for linear global control " <<
            pid_distance.Kp_ << " " << pid_distance.Ki_ << " " << pid_distance.Kd_);
    ROS_CONTROL_STREAM("Coefficients for angular global control " <<
            pid_orientation.Kp_ << " " << pid_orientation.Ki_ << " " << pid_orientation.Kd_);
    ROS_CONTROL_STREAM("Coefficients for linear potential control " <<
            pid_cmd_vel_linear.Kp_ << " " << pid_cmd_vel_linear.Ki_ << " " << pid_cmd_vel_linear.Kd_);
    ROS_CONTROL_STREAM("Coefficients for angular potential control " <<
            pid_cmd_vel_angular.Kp_ << " " << pid_cmd_vel_angular.Ki_ << " " << pid_cmd_vel_angular.Kd_);
    if (!omnidirectional)
        ROS_CONTROL_STREAM("Correction coefficient " << correction_coefficient);

    current_pose = Pose2{0.0,0.0,0.0};

    tn_1_secs = tn_secs = 0.;

    if (simulation_active)
        ROS_CONTROL_STREAM("Simulation control active");

    mode = Mode::global;

    ROS_CONTROL_STREAM("Node Control starting");
}


//================================================================================
// CALLBACKS
//================================================================================


/* odometryCallback
 * input:
 *  r2robotics_msgs::Position::ConstPtr& feedback, feedback from odometry (pose and speed)
 * output:
 *     none
 * description:
 *     This function gets the feedback from the odometry to compute the control laws.
 */
void Control::odometryCallback(const r2robotics_msgs::Position::ConstPtr& feedback)
{
    tn_secs = ros::Time::now().toSec();

    std::vector<float> position{feedback->x * 1e-3, feedback->y * 1e-3};

    blockingDetection(position);

    current_pose.x = feedback->x * 1e-3;
    current_pose.y = feedback->y * 1e-3;
    current_pose.yaw = feedback->theta; // comment this line to prevent dynamic change of steering

    int i = 0;
    for (auto wh: wheels)
    {
        wheels_speeds[i] = feedback->rawSpeeds[i];
        ++i;
    }

    if (emergency_stop)
        return;

    timeout();

    switch(mode){
        case Mode::global:
        {
            pid_distance.feedback_tn = distance(initial_pose, current_pose);
            //blockingDetectionStraightLine(pid_distance.feedback_tn);
            pid_distance.updateTime(tn_secs);

            double y_ = std::fmod((current_pose.yaw - initial_pose.yaw + M_PI + 2 * M_PI), 2 * M_PI) - M_PI;

            pid_orientation.feedback_tn = y_;
            pid_orientation.updateTime(tn_secs);

            if (pid_distance.instruction_tn != 0.0) cmdVelCompute(pid_distance);
            if (pid_orientation.instruction_tn != 0.0) cmdVelCompute(pid_orientation);

            wheelsCmdConversion(pid_distance, pid_orientation);
        }
        break;
        case Mode::potential:
        {
            pid_cmd_vel_linear.updateTime(tn_secs);
            pid_cmd_vel_angular.updateTime(tn_secs);

            double sum = 0.0;
            int i = 0;

            if (pid_cmd_vel_linear.instruction_tn != 0.0 || pid_cmd_vel_angular.instruction_tn != 0.0)
            {
                cmdVelCompute(pid_cmd_vel_linear);
                cmdVelCompute(pid_cmd_vel_angular);

                wheelsCmdConversion(pid_cmd_vel_linear, pid_cmd_vel_angular);
            }

            if (pid_cmd_vel_angular.instruction_tn != 0.0)
            {
                for (auto &w: wheels)
                {
                    sum += (pid_cmd_vel_angular.command_tn != 0 ?
                            feedback->rawSpeeds[i] / w.getDistanceToRobotCenter() :
                            0.0) *
                                    1e-3;
                    ++i;
                }

                pid_cmd_vel_angular.feedback_tn = sum / static_cast<double>(i);

                cmdVelCompute(pid_cmd_vel_angular);

                wheelsCmdConversion(pid_cmd_vel_linear, pid_cmd_vel_angular);

            }

            i = 0;

            if (pid_cmd_vel_linear.instruction_tn != 0.0)
            {
                i = 0;

                for(auto &w: wheels)
                {
                    sum += (pid_cmd_vel_linear.command_tn != 0.0 ?
                            std::fabs(feedback->rawSpeeds[i]) : 0.0) * 1e-3;
                    ++i;
                }

                double number = static_cast<double>(i);

                pid_cmd_vel_linear.setFeedback(sum / number);

                cmdVelCompute(pid_cmd_vel_linear);
                wheelsCmdConversion(pid_cmd_vel_linear, pid_cmd_vel_angular);
            }
        }
        break;
        default:
        break;
    }
}

void Control::instructionCallback(const r2robotics_msgs::Movement::ConstPtr& instruction)
{ //FILTERING INSTRUCTION
    if (emergency_stop)
        return;

    mode = Mode::global;

    waypoint_reached = false;

    resetStop();
    time_not_moving = 0;

    //movement stuff
    movement_mode = static_cast<Movement>(instruction->mode);
    std::vector<float> pos_to_reach = instruction->position_to_reach;
    Position2 rotation_center_position{};
    double final_yaw = instruction->final_yaw;
    int direction = instruction->direction;

    if (instruction->rotation_center_position.size() >= 2)
    {
        rotation_center_position = (instruction->rotation_center_position);
    }

    //time stuff
    tn_secs = ros::Time::now().toSec();
    time_beginning_movement = tn_secs;

    initial_pose = current_pose;

    pid_distance.reset();
    pid_distance.feedback_tn = 0.0;
    pid_distance.updateTime(tn_secs);

    pid_orientation.reset();
    pid_orientation.feedback_tn = 0.0;
    pid_orientation.updateTime(tn_secs);

    cmdVelInstructionCompute(pos_to_reach, rotation_center_position, final_yaw, direction);

    cmdVelCompute(pid_distance);
    cmdVelCompute(pid_orientation);

    ROS_CONTROL_STREAM("command computed " << pid_distance.command_tn_derivate << " " << pid_orientation.command_tn_derivate);

    if (simulation_active)
    {
        cmdVelPublish(pid_distance, pid_orientation);
    } else
    {
        wheelsCmdConversion(pid_distance, pid_orientation);
    }
}

/* gazeboCallback
 * input:
 *  gazebo_msgs::LinkStates::ConstPtr& feedback, feedback from the simulator, gives position and speed
 * output:
 *  none
 * description:
 *  Callback function for Gazebo simulation, acts as an odometry feedback
 * */
void Control::gazeboCallback(const gazebo_msgs::LinkStates::ConstPtr& feedback)
{
    if (emergency_stop)
        return;

    auto names = &(feedback->name);
    auto it = std::find(names->begin(), names->end(), "gr::base_link");

    if (it == names->end()) return;

    int i = it - names->begin();

    tn_secs = ros::Time::now().toSec();

    current_pose = Pose2{
        feedback->pose[i].position.x,
        feedback->pose[i].position.y,
        asin(
                sqrt(
                        sqr(feedback->pose[i].orientation.x) +
                        sqr(feedback->pose[i].orientation.y) +
                        sqr(feedback->pose[i].orientation.z)
                    )
            )*2};

    switch(mode)
    {
        case Mode::potential:
        {
            pid_cmd_vel_linear.updateTime(tn_secs);
            pid_cmd_vel_angular.updateTime(tn_secs);

            pid_cmd_vel_linear.feedback_tn =
                    sqrt(sqr(feedback->twist[i].linear.x) +
                    sqr(feedback->twist[i].linear.y));

            pid_cmd_vel_angular.feedback_tn = feedback->twist[i].angular.z;

            cmdVelCompute(pid_cmd_vel_linear);
            cmdVelCompute(pid_cmd_vel_angular);

            if (simulation_active)
            {
                cmdVelPublish(pid_cmd_vel_linear, pid_cmd_vel_angular);
            } else
            {
                wheelsCmdConversion(pid_cmd_vel_linear, pid_cmd_vel_angular);
            }
        }
        break;
        case Mode::global:
        {
            pid_distance.updateTime(tn_secs);
            pid_orientation.updateTime(tn_secs);

            pid_distance.feedback_tn = distance(initial_pose, current_pose);
            pid_orientation.feedback_tn = current_pose.yaw - initial_pose.yaw;

            cmdVelCompute(pid_distance);
            cmdVelCompute(pid_orientation);

            if (simulation_active)
            {
                cmdVelPublish(pid_distance, pid_orientation);
            } else
            {
                wheelsCmdConversion(pid_distance, pid_orientation);
            }
        }
        break;
        default:
        break;
    }

}

/* velocityCallback
 * input:
 *  r2robotics_msgs::Velocity::ConstrPtr& vel_instruction: velocities instruction from trajectory
 * output:
 *  none
 * description:
 *  This function stores the new instruction to compute the right command to send to the wheels
 * */
void Control::velocityCallback(const r2robotics_msgs::Velocity::ConstPtr& vel_instruction)
{
    if (emergency_stop)
        return;

    mode = Mode::potential;

    tn_secs = ros::Time::now().toSec();

    pid_cmd_vel_linear.reset();
    pid_cmd_vel_angular.reset();

    pid_cmd_vel_linear.updateTime(tn_secs);
    pid_cmd_vel_angular.updateTime(tn_secs);

    pid_cmd_vel_linear.reset();
    pid_cmd_vel_angular.reset();

    movement_mode = Movement::crab;

    if (vel_instruction->dx != 0.0 || vel_instruction->dy != 0.0)
    {
        double velocity_norm = sqrt(sqr(vel_instruction->dx) + sqr(vel_instruction->dy));

        pid_cmd_vel_linear.instruction_tn =
                std::min(velocity_norm, pid_cmd_vel_linear.max_speed);

        wanted_direction = atan2(-vel_instruction->dy, vel_instruction->dx);
    } else
    {
        pid_cmd_vel_linear.instruction_tn = 0.0;
    }

    if (vel_instruction->dyaw != 0.0)
    {
        pid_cmd_vel_angular.instruction_tn = vel_instruction->dyaw /
                fabs(vel_instruction->dyaw) *
                std::min(
                        fabs(static_cast<double>(vel_instruction->dyaw)),
                        pid_cmd_vel_angular.max_speed
                        );

        movement_mode = static_cast<Movement>(movement_mode + 1);
    }
    else
    {
        pid_cmd_vel_angular.instruction_tn = 0.0;
    }

    if (movement_mode == Movement::arc_circle)
    {
        double r = sqrt(sqr(vel_instruction->dx) + sqr(vel_instruction->dy)) / fabs(vel_instruction->dyaw);
        Position2 vect_to_center{r * vel_instruction->dy, r * vel_instruction->dx};
        vel_instruction->dyaw > 0 ? vect_to_center.y *= -1.0 : vect_to_center.x *= -1.0;
        instant_rotation_center = vect_to_center;
    }

    cmdVelCompute(pid_cmd_vel_linear);
    cmdVelCompute(pid_cmd_vel_angular);

    if (simulation_active)
    {
        cmdVelPublish(pid_cmd_vel_linear, pid_cmd_vel_angular);
    } else
    {
        wheelsCmdConversion(pid_cmd_vel_linear, pid_cmd_vel_angular);
    }
}


//================================================================================
// COMMAND COMPUTATION
//================================================================================

void Control::cmdVelCompute(PID& pid)
{//CALCULATE COMMAND WITH FEEDBACK AND INSTRUCTION
    if (emergency_stop)
        return;

       pid.computePID();

       switch(mode)
       {
        case Mode::potential:
        {
            if(std::fabs(pid.command_tn) < pid.min_speed || pid.instruction_tn == 0.0)
            {
                pid.command_tn = 0.0;
            }
        }
        break;
        case Mode::global:
        {
            if(std::fabs(pid.command_tn_derivate) < pid.min_speed ||
        	    (std::fabs(pid.instruction_tn) <= 0.015 && movement_mode == Movement::crab))
            {
                pid.command_tn_derivate = 0.0;
            }
            if ((std::fabs(pid.command_tn_derivate) == 0.0 && pid.instruction_tn != 0.0) || std::fabs(pid.error) < 0.005)
            {
                pid.command_tn_derivate = 0.0;
                incrementStop();
                if(time_stop == 10)
                {
                    client_waypoint.call(srv_waypoint);
                    waypoint_reached = true;
                    ROS_CONTROL_STREAM("Waypoint reached");
                }
            }
        }
        break;
        default:
        break;
       }

    return;
}


/* cmdVelInstructionCompute
 * inputs:
 *  int& mode, std:vector<float>& pos_to_reach, Position2 rotation_center_position, double& yaw, int& direction
 * output:
 *  none
 * description:
 *     This function computes the instruction to send to the PID controller for the expected movement (arc circle, straight, rotation)
 */
void Control::cmdVelInstructionCompute(
        std::vector<float>& pos_to_reach,
        Position2 rotation_center_position,
        double& yaw,
        int& direction)
{
    if (emergency_stop)
        return;

    switch (movement_mode)
    {
        case Movement::arc_circle:
        {
            double distance_to_center;
            distance_to_center = distance(current_pose, rotation_center_position);

            Position2 vect_direction;
            vect_direction = computeVector(rotation_center_position, current_pose);

            if (omnidirectional)
            {
                std::vector<float> angles {};
                for(auto& wh: wheels)
                {
                    auto v_ = computeVector(rotation_center_position, wh.getPosition());
                    ROS_CONTROL_STREAM("Vector computed " << v_.x << " " << v_.y);
                    angles.push_back(modulo2PI(std::atan2(-v_.x, v_.y) * 180 / M_PI));
                }

                srv_direction.request.time = 10;
                srv_direction.request.angles = angles;

                if (client_steering.call(srv_direction))
                {
                    int i = 0;
                    for (auto& wh: wheels)
                    {
                        wh.updateOrientation(srv_direction.request.angles[i]);
                        ++i;
                    }
                }

                ROS_CONTROL_STREAM("Angles for arc circle " << angles [0] << " " << angles[1] << " " << angles[2]);
            }


            wanted_direction = std::atan2(vect_direction.x, -vect_direction.y);
            wanted_direction = (!omnidirectional && direction < 0 ? current_pose.yaw : current_pose.yaw + M_PI);

            pid_distance.instruction_tn = (yaw - current_pose.yaw) * distance_to_center;
            pid_orientation.instruction_tn = yaw - current_pose.yaw;

            ROS_CONTROL_STREAM("ARC CIRCLE computed " << pid_distance.instruction_tn << ", direction " << wanted_direction);

            instant_rotation_center = rotation_center_position;
        }
        break;
        case Movement::crab:
        {
            Position2 vect_direction = computeVector(pos_to_reach, current_pose);
            wanted_direction = std::atan2(vect_direction.y, vect_direction.x);
            float orientation = wanted_direction + current_pose.yaw;

            if (omnidirectional)
            {
                std::vector<float> angles {};
                for(auto& wh: wheels)
                {
                    angles.push_back(modulo2PI(orientation * 180 / M_PI));
                }

                srv_direction.request.time = 10;
                srv_direction.request.angles = angles;

                if (client_steering.call(srv_direction))
                {
                    int i = 0;
                    for (auto& wh: wheels)
                    {
                        wh.updateOrientation(srv_direction.request.angles[i]);
                        ++i;
                    }
                }
            }

            ros::Duration(0.5).sleep();

            pid_distance.instruction_tn = vect_direction.norm();

            if (pid_distance.instruction_tn < 0.015)
        	client_waypoint.call(srv_waypoint);

            pid_orientation.instruction_tn = 0;

            ROS_CONTROL_STREAM("STRAIGHT computed " << pid_distance.instruction_tn << ", direction " << wanted_direction);

            instant_rotation_center = Position2{0.,0.};
        }
        break;
        case Movement::on_place:
        {
            if (omnidirectional)
            {
                client_rotation.call(srv_rotation);
            }

            ros::Duration(0.5).sleep();

            double y_ = std::fmod((yaw - initial_pose.yaw + M_PI + 2 * M_PI), 2 * M_PI) - M_PI;

            pid_distance.instruction_tn = 0.0;
            pid_orientation.instruction_tn = y_;

            ROS_CONTROL_STREAM("ROTATION computed " << pid_orientation.instruction_tn << ", direction " << wanted_direction);

            instant_rotation_center = Position2{0.,0.};
        }
        break;
        default:
        {
            pid_distance.instruction_tn = 0.0;
            pid_orientation.instruction_tn = 0.;

            instant_rotation_center = Position2{0.,0.};
        break;
        }
    }

     return;
}

//================================================================================
// COMMAND PUBLICATION
//================================================================================

/* cmdVelPublish
 * inputs:
 *  PID& pid1, PID& pid2; PID& pid3: 3 PID controls corresponding respectively to x, y and yaw velocity/position wrt the control mode
 * output:
 *  none
 * description:
 *  This function publishes the different velocities on the adapted topic.
 * */
void Control::cmdVelPublish(PID& pid1, PID& pid2)
{
    switch(mode)
    {
        case Mode::potential:
        {
            cmd_vel.linear.x = pid1.command_tn * cos(wanted_direction - current_pose.yaw);
            cmd_vel.linear.y = pid1.command_tn * sin(wanted_direction - current_pose.yaw);
            cmd_vel.linear.z = 0.;

            cmd_vel.angular.x = 0;
            cmd_vel.angular.y = 0;
            cmd_vel.angular.z = pid2.command_tn;
        }
        break;
        case Mode::global:
        {
            cmd_vel.linear.x = pid1.command_tn_derivate * cos(wanted_direction - current_pose.yaw);
            cmd_vel.linear.y = pid1.command_tn_derivate * sin(wanted_direction - current_pose.yaw);
            cmd_vel.linear.z = 0.;

            cmd_vel.angular.x = 0.;
            cmd_vel.angular.y = 0.;
            cmd_vel.angular.z = pid2.command_tn_derivate;
        }
        break;
        default:
        break;
    }

    cmd_vel_.publish(cmd_vel);
}

/* wheelsCmdPublish
 * inputs:
 *  wheels_cmd: vector of linear speed of each wheel in its direction (unit: m/s)
 * output:
 *  none
 * description:
 *  This function computes the needed rotation speed for the wheels to reach the expected linear speed and publishes the commands to the wheels.
 * */
void Control::wheelsCmdPublish(std::vector<double> wheels_cmd, bool direction)
{
    r2robotics_msgs::motors_speed_request pub;
    std::vector<int> commands;
    int i = 0;
    bool sign_differential = true;

    for (auto wh:wheels)
    {
        pub.speeds.push_back(abs(static_cast<int>(wheels_cmd[i] /(M_PI * wh.radius) * wh.reduction_factor * 60.0)));
        pub.directions.push_back(omnidirectional ?
                direction : (sign_differential ? direction : !direction));
        sign_differential = (movement_mode != Movement::on_place ?
                !sign_differential : sign_differential);
        pub.enables.push_back(true);
        pub.idMotors.push_back(i+1);
        ++i;
    }

    cmd_wheels_.publish(pub);
}

//================================================================================
// COMMAND CONVERSION
//================================================================================

/* wheelsCmdConversion
 * inputs:
 *  PID& pid1, PID& pid2; PID& pid3: 3 PID controls corresponding respectively to x, y and yaw velocity/position wrt the control mode
 * output:
 *  none
 * description:
 *  This function computes the respective linear speeds for each wheel wrt the control mode..
 * */
void Control::wheelsCmdConversion(PID& pid1, PID& pid2)
{
    if (emergency_stop)
        return;

    float vx, vy, w;

    switch(mode)
    {
        case Mode::potential:
        {
            auto thresholding = [](PID& pid)
            {
                return std::max(std::min(pid.command_tn, pid.max_speed), -pid.max_speed);
            };

            vx = thresholding(pid1) * cos(wanted_direction - current_pose.yaw);
            vy = thresholding(pid1) * sin(wanted_direction - current_pose.yaw);
            w = thresholding(pid2);
        }
        break;
        case Mode::global:
        {
            auto thresholding = [](PID& pid)
            {
                return std::max(std::min(pid.command_tn_derivate, pid.max_speed), -pid.max_speed);
            };

            vx = thresholding(pid1) * cos(wanted_direction);
            vy = thresholding(pid1) * sin(wanted_direction);
            w = thresholding(pid2);
        }
        break;
        case Mode::recalibration:
        {
            auto thresholding = [](PID& pid)
            {
                return std::max(std::min(pid.command_tn, pid.max_speed), -pid.max_speed);
            };

            vx = thresholding(pid1) * cos(wanted_direction);
            vy = thresholding(pid1) * sin(wanted_direction);
            w = thresholding(pid2);
        }
        break;
        default:
        break;
    }
    // ARC CIRCLE MODE
    switch(movement_mode)
    {
        case Movement::arc_circle:
        {
            std::vector<float> angles;
            std::vector<double> wheel_speeds;

            bool direction;

            if (omnidirectional)
            {
                std::vector<float> angles {};
                for(auto& wh: wheels)
                {
                    auto v_ = computeVector(wh.getPosition(), instant_rotation_center);
                    angles.push_back(modulo2PI(std::atan2(-v_.x, v_.y) * 180 / M_PI));
                    wheel_speeds.push_back(v_.norm() * w);
                }

                srv_direction.request.time = 10;
                srv_direction.request.angles = angles;

                if (client_steering.call(srv_direction))
                {
                    int i = 0;
                    for (auto& wh: wheels)
                    {
                        wh.updateOrientation(srv_direction.request.angles[i]);
                        ++i;
                    }
                }
            }
            else
            {
                for(auto& wh:wheels)
                {
                    auto vect_wheel_rc = displacement(instant_rotation_center, wh.getPosition());
                    auto normalized = vect_wheel_rc.getNormalized();
                    wheel_speeds.push_back(vect_wheel_rc.norm() * w);
                }
            }

            direction = false;
            if(mode == Mode::global && std::fabs(std::fabs(wanted_direction)- std::fabs(M_PI)) < 0.1)
                direction = !direction;

            if (mode != Mode::potential)
                direction = (pid2.command_tn_derivate < 0 ? !direction : direction);

            normalizeAndThresholding(wheel_speeds, pid_distance.max_speed);

            wheelsCmdPublish(wheel_speeds, direction);
        }
        break;
        case Movement::on_place:
        {
            std::vector<double> wheel_speeds;
            bool direction = true;
            if (omnidirectional)
            {
                if (!client_rotation.call(srv_rotation))
                {
                    ROS_ERROR("Service not running");
                }
            }

            int i = 0;
            for(auto& wh: wheels)
            {
                wh.updateOrientation(0.0);
                wheel_speeds.push_back(wh.getDistanceToRobotCenter() * w);
                ++i;
            }

            if (mode != Mode::potential)
                direction = (pid2.command_tn_derivate < 0 ? !direction : direction);

            wheelsCmdPublish(wheel_speeds, direction);
        }
        break;
        case Movement::crab:
        {
            std::vector<float> angles;
            srv_direction.request.time = 10;
            float orientation = wanted_direction - current_pose.yaw;
            bool direction;

            if (omnidirectional)
            {
                for(auto& wh: wheels)
                {
                    angles.push_back(modulo2PI(orientation * 180 / M_PI));
                }

                srv_direction.request.angles = angles;

                if (client_steering.call(srv_direction))
                {
                    int i = 0;
                    for (auto& wh: wheels)
                    {
                        wh.updateOrientation(srv_direction.request.angles[i]);
                        ++i;
                    }
                    direction = (!(std::fabs(srv_direction.request.angles[0] - srv_direction.response.angles[0]) < 5.));
                }
                else
                {
                    ROS_ERROR("Service not running");
                    direction = vx / std::fabs(vx);
                }
            }

            float norm = sqrt(sqr(vx) + sqr(vy));
            std::vector<double> commands(wheels.size(), norm);

            if (omnidirectional)
            {
                if (vx < 0.0 && std::fabs(std::fabs(orientation) - std::fabs(M_PI)) < 0.02 && mode == Mode::potential)
                    direction = !direction;

                if(mode == Mode::global && std::fabs(std::fabs(orientation)- std::fabs(M_PI)) < 0.02)
                    direction = false;

                if (mode == Mode::global)
                    direction = (pid1.command_tn_derivate < 0 &&
                            std::fabs(std::fabs(orientation)- std::fabs(M_PI)) > 0.02 ? !direction : direction);
            } else
            {
                direction = false;

                float error = current_pose.yaw - wanted_direction;

                if(mode != Mode::potential && std::fabs(std::fabs(error)- std::fabs(M_PI)) < 0.1)
                    direction = !direction;

                if (std::fabs(error - M_PI) <= std::fabs(error)) error -= M_PI;
                if (std::fabs(error + M_PI) <= std::fabs(error)) error += M_PI;

                if (mode != Mode::potential)
                    direction = (pid1.command_tn_derivate < 0 ? !direction : direction);

                if (wheels_speeds[0] != 0. && wheels_speeds[1] != 0.)
                {
                    correctOrientation(error, 0, 1, direction, commands);
                }


            }

            wheelsCmdPublish(commands, direction);
        }
        break;
        default:
            wheelsCmdPublish({0.0,0.0,0.0}, true);
        break;
    }

    return;
}

/* correctOrientation
 * inputs:
 *  double error, int index_left_wheel, int index_right_wheel, bool direction, std::vector<double>& commands
 * output:
 *  none
 * description:
 *  This function computes the correction to apply to each wheel speeds according to the yaw error and the direction of the robot to
 *  follow a straight line
 *  */

void Control::correctOrientation(double error, int index_left_wheel, int index_right_wheel, bool direction, std::vector<double> &commands){
    if (error > 0)
    {
        if (!direction)
        {
            commands[index_right_wheel] = commands[index_right_wheel] * cos(correction_coefficient * error);
        }
        else
        {
            commands[index_left_wheel] = commands[index_left_wheel] * cos(correction_coefficient * error);
        }
    } else {
        if (direction)
        {
            commands[index_right_wheel] = commands[index_right_wheel] * cos(correction_coefficient * error);
        }
        else
        {
            commands[index_left_wheel] = commands[index_left_wheel] * cos(correction_coefficient * error);
        }
    }
}

//================================================================================
// STOP TIME MANAGEMENT
//================================================================================

void Control::incrementStop(){
    ++time_stop;
}

void Control::resetStop(){
    time_stop = 0;
}

void Control::blockingDetection(std::vector<float> position){

    if((distance(position, current_pose) < 0.005 ||
            (distance(position, initial_pose) > 0.05 && movement_mode == Movement::on_place))
	    && (pid_distance.instruction_tn != 0.0 || pid_orientation.instruction_tn != 0.0))
    {
        ++time_not_moving;
    } else
    {
        time_not_moving = 0;
    }

    if (time_not_moving == 10)
    {
        std_msgs::Empty msg;
        blocking_detected_.publish(msg);
        ROS_CONTROL_STREAM("Blocking detected");
    }
}

void Control::blockingDetectionStraightLine(double distance){

    Position2 expected_position{};
    expected_position.x = initial_pose.x + cos(wanted_direction + current_pose.yaw) * distance;
    expected_position.y = initial_pose.y + sin(wanted_direction + current_pose.yaw) * distance;

    auto delta = computeVector(expected_position, current_pose);

    if (delta.norm() > 0.01)
	++time_not_moving;

    if (time_not_moving == 50)
    {
        std_msgs::Empty msg;
        blocking_detected_.publish(msg);
        ROS_CONTROL_STREAM("Blocking detected");
    }

}

void Control::timeout()
{
    if (!waypoint_reached && tn_secs - time_beginning_movement > timeout_duration &&
	    (pid_distance.instruction_tn != 0.0 || pid_orientation.instruction_tn != 0.0))
    {
        std_msgs::Empty msg;
        timeout_.publish(msg);
        ROS_CONTROL_STREAM("Timeout detected");
        r2robotics_srvs::stopControl::Request req;
        r2robotics_srvs::stopControl::Response res;
        stopControl(req,res);
    }
}



//================================================================================
// SERVICES
//================================================================================

bool Control::setMaxLinearSpeed(r2robotics_srvs::setControlMaxLinearSpeed::Request &req,
                          r2robotics_srvs::setControlMaxLinearSpeed::Response &res)
{
    switch (mode)
    {
        case Mode::potential:
            pid_cmd_vel_linear.max_speed = req.speed;
            break;
        case Mode::global:
            pid_distance.max_speed = req.speed;
        default:
            break;
    }

    ROS_CONTROL_STREAM("Maximum linear speed set to " <<
            (mode == Mode::potential ?
                    pid_cmd_vel_linear.max_speed :
                    pid_distance.max_speed));
    return true;
}

bool Control::setMaxAngularSpeed(r2robotics_srvs::setControlMaxAngularSpeed::Request &req,
                          r2robotics_srvs::setControlMaxAngularSpeed::Response &res)
{
    switch (mode)
    {
        case Mode::potential:
            pid_cmd_vel_angular.max_speed = req.speed;
            break;
        case Mode::global:
            pid_orientation.max_speed = req.speed;
        default:
            break;
    }

    ROS_CONTROL_STREAM("Maximum angular speed set to " <<
            (mode == Mode::potential ?
                    pid_cmd_vel_angular.max_speed :
                    pid_orientation.max_speed));
    return true;
}

bool Control::setMinLinearSpeed(r2robotics_srvs::setControlMinLinearSpeed::Request &req,
                          r2robotics_srvs::setControlMinLinearSpeed::Response &res)
{
    switch (mode)
    {
        case Mode::potential:
            pid_cmd_vel_linear.min_speed = req.speed;
            break;
        case Mode::global:
            pid_distance.min_speed = req.speed;
        default:
            break;
    }

    ROS_CONTROL_STREAM("Maximum linear speed set to " <<
            (mode == Mode::potential ?
                    pid_cmd_vel_linear.min_speed :
                    pid_distance.min_speed));
    return true;
}

bool Control::setMinAngularSpeed(r2robotics_srvs::setControlMinAngularSpeed::Request &req,
                          r2robotics_srvs::setControlMinAngularSpeed::Response &res)
{
    switch (mode)
    {
        case Mode::potential:
            pid_cmd_vel_angular.min_speed = req.speed;
            break;
        case Mode::global:
            pid_orientation.min_speed = req.speed;
        default:
            break;
    }

    ROS_CONTROL_STREAM("Maximum angular speed set to " <<
            (mode == Mode::potential ?
                    pid_cmd_vel_angular.min_speed :
                    pid_orientation.min_speed));
    return true;
}

bool Control::stopControl(r2robotics_srvs::stopControl::Request &req,
                 r2robotics_srvs::stopControl::Response &res)
{
    std::vector<double> stop(wheels.size(), 0.);
    wheelsCmdPublish(stop, true);

    emergency_stop = true;

    switch(mode)
    {
        case Mode::global:
            pid_distance.reset();
            pid_distance.reset();
        break;
        case Mode::potential:
            pid_cmd_vel_linear.reset();
            pid_cmd_vel_angular.reset();
        break;
        default:
        break;
    }

    ROS_CONTROL_STREAM("Control stopped");
    return true;
}


bool Control::resumeControl(r2robotics_srvs::resumeControl::Request &req,
                 r2robotics_srvs::resumeControl::Response &res)
{
    if (!req.resume_last_command)
    {
        switch(mode)
        {
            case Mode::global:
                pid_distance.instruction_tn = 0.0;
                pid_distance.reset();
                pid_orientation.instruction_tn = 0.0;
                pid_distance.reset();
            break;
            case Mode::potential:
                pid_cmd_vel_linear.instruction_tn = 0.0;
                pid_cmd_vel_linear.reset();
                pid_cmd_vel_angular.instruction_tn = 0.0;
                pid_cmd_vel_angular.reset();
            break;
            default:
            break;
        }
    }
    emergency_stop = false;

    time_beginning_movement = tn_secs;

    ROS_CONTROL_STREAM("Control resumed");
    return true;
}

bool Control::startRecalibration(r2robotics_srvs::startRecalibration::Request &req,
                 r2robotics_srvs::startRecalibration::Response &res)
{
    auto prev_mode = mode;
    previous_mode = mode;
    time_not_moving = 0;
    switch(mode)
    {
        case Mode::global:
            pid_distance.instruction_tn = 0.0;
            pid_distance.reset();
            pid_orientation.instruction_tn = 0.0;
            pid_distance.reset();
        break;
        case Mode::potential:
            pid_cmd_vel_linear.instruction_tn = 0.0;
            pid_cmd_vel_linear.reset();
            pid_cmd_vel_angular.instruction_tn = 0.0;
            pid_cmd_vel_angular.reset();
        break;
        default:
        break;
    }
    wanted_direction = std::atan2(req.y, req.x) ;

    mode = Mode::recalibration;

    PID pid{};
    PID none{};

    pid.command_tn = 0.5;
    wheelsCmdConversion(pid, none);

    res.done = true;
    return true;
}

bool Control::stopRecalibration(r2robotics_srvs::stopRecalibration::Request &req,
                 r2robotics_srvs::stopRecalibration::Response &res)
{
    mode = previous_mode;
    switch(mode)
    {
        case Mode::global:
            pid_distance.instruction_tn = 0.0;
            pid_distance.reset();
            pid_orientation.instruction_tn = 0.0;
            pid_distance.reset();
        break;
        case Mode::potential:
            pid_cmd_vel_linear.instruction_tn = 0.0;
            pid_cmd_vel_linear.reset();
            pid_cmd_vel_angular.instruction_tn = 0.0;
            pid_cmd_vel_angular.reset();
        break;
        default:
        break;
    }

    return true;
}

bool Control::setTimeout(r2robotics_srvs::setControlTimeout::Request &req,
                           r2robotics_srvs::setControlTimeout::Response &res)
{
    timeout_duration = req.time;

    return true;
}




//================================================================================
// INITIALISATION SETTERS
//================================================================================

void Control::setSubscribers()
{
    gazebo_sub_ = nh_.subscribe<gazebo_msgs::LinkStates>("/gazebo/link_states", 10, &Control::gazeboCallback, this);
    odometry_sub_ = nh_.subscribe<r2robotics_msgs::Position>("odometry_feedback", 1, &Control::odometryCallback, this);
    velocity_sub_ = nh_.subscribe<r2robotics_msgs::Velocity>("velocity_control", 10, &Control::velocityCallback, this);
    instruction_sub_ = nh_.subscribe<r2robotics_msgs::Movement>("global_control", 10, &Control::instructionCallback, this);
}

void Control::setPublishers()
{
    cmd_vel_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
    cmd_wheels_ = nh_.advertise<r2robotics_msgs::motors_speed_request>("/motors_speed_request", 1);
    blocking_detected_ = nh_.advertise<std_msgs::Empty>("/blocking_detected",1);
    timeout_ = nh_.advertise<std_msgs::Empty>("/control_timeout",1);

}

void Control::setClients()
{
    client_waypoint = nh_.serviceClient<std_srvs::Trigger>("waypoint_reached");
    client_steering = nh_.serviceClient<r2robotics_srvs::move_turrets_robot_frame>("moveTurretsRobotFrame");
    client_rotation = nh_.serviceClient<r2robotics_srvs::set_self_rotation>("setSelfRotation");
}

void Control::setServices()
{
    set_max_linear_speed = nh_.advertiseService("set_max_linear_speed", &Control::setMaxLinearSpeed, this);
    set_max_angular_speed = nh_.advertiseService("set_max_angular_speed", &Control::setMaxAngularSpeed, this);
    set_min_linear_speed = nh_.advertiseService("set_min_linear_speed", &Control::setMaxLinearSpeed, this);
    set_min_angular_speed = nh_.advertiseService("set_min_angular_speed", &Control::setMaxAngularSpeed, this);
    stop_control = nh_.advertiseService("stop_control", &Control::stopControl, this);
    resume_control = nh_.advertiseService("resume_control", &Control::resumeControl, this);
    start_recalibration = nh_.advertiseService("start_recalibration", &Control::startRecalibration, this);
    stop_recalibration = nh_.advertiseService("stop_recalibration", &Control::stopRecalibration, this);
    set_timeout = nh_.advertiseService("set_control_timeout", &Control::setTimeout, this);
}



//================================================================================
// PARAMETERS GETTERS
//================================================================================

void Control::getParams()
{

    //make it a parameter
    //PID wheel_coefficient(0.9, 0.8, 0.00005);
    double kpp, kip, kdp;
    double min_linear_speed, min_angular_speed, max_linear_speed, max_angular_speed;
    double kpg_linear, kig_linear, kdg_linear, kpg_angular, kig_angular, kdg_angular;

    std::string common_string =
            (ros::this_node::getNamespace() != "/" ? ros::this_node::getNamespace() + "/" :
                    "") + ros::this_node::getName();

    simulation_active = ros::param::has("/use_sim_time");

    if (!simulation_active)
    {
        ros::param::has(common_string + "/global/linear/kp") ?
                ros::param::get(common_string +  "/global/linear/kp", kpg_linear) :
                kpg_linear = 10.8;
        ros::param::has(common_string + "/global/linear/ki") ?
                ros::param::get(common_string +  "/global/linear/ki", kig_linear) :
                kig_linear = 10.8;
        ros::param::has(common_string + "/global/linear/kd") ?
                ros::param::get(common_string +  "/global/linear/kd", kdg_linear) :
                kdg_linear = 10.8;

        ros::param::has(common_string +  "/global/angular/kp") ?
                ros::param::get(common_string +  "/global/angular/kp", kpg_angular) :
                kpg_angular = 10.8;
        ros::param::has(common_string +  "/global/angular/ki") ?
                ros::param::get(common_string +  "/global/angular/ki", kig_angular) :
                kig_angular = 10.8;
        ros::param::has(common_string +  "/global/angular/kd") ?
                ros::param::get(common_string +  "/global/angular/kd", kdg_angular) :
                kdg_angular = 10.8;

        ros::param::has(common_string +  "/potential/kp") ?
                ros::param::get(common_string +  "/potential/kp", kpp) :
                kpp = 0.9;
        ros::param::has(common_string +  "/potential/ki") ?
                ros::param::get(common_string +  "/potential/ki", kip) :
                kip = 0.8;
        ros::param::has(common_string +  "/potential/kd") ?
                ros::param::get(common_string +  "/potential/kd", kdp) :
                kdp = 5e-5;
    } else
    {
        ros::param::has("/control_simulation/global/linear/kp") ?
                ros::param::get("/control_simulation/global/linear/kp", kpg_linear) :
                kpg_linear = 10.8;
        ros::param::has("/control_simulation/global/linear/ki") ?
                ros::param::get("/control_simulation/global/linear/ki", kig_linear) :
                kig_linear = 10.8;
        ros::param::has("/control_simulation/global/linear/kd") ?
                ros::param::get("/control_simulation/global/linear/kd", kdg_linear) :
                kdg_linear = 10.8;

        ros::param::has("/control_simulation/global/angular/kp") ?
                ros::param::get("/control_simulation/global/linear/kp", kpg_angular) :
                kpg_angular = 10.8;
        ros::param::has("/control_simulation/global/angular/ki") ?
                ros::param::get("/control_simulation/global/linear/ki", kig_angular) :
                kig_angular = 10.8;
        ros::param::has("/control_simulation/global/angular/kd") ?
                ros::param::get("/control_simulation/global/linear/kd", kdg_angular) :
                kdg_angular = 10.8;

        ros::param::has("/control_simulation/potential/kp") ?
                ros::param::get("/control_simulation/potential/kp", kpp) :
                kpp = 0.9;
        ros::param::has("/control_simulation/potential/ki") ?
                ros::param::get("/control_simulation/potential/ki", kip) :
                kip = 0.8;
        ros::param::has("/control_simulation/potential/kd") ?
                ros::param::get("/control_simulation/potential/kd", kdp) :
                kdp = 5e-5;
    }

    ros::param::has(common_string +  "/min_linear_speed") ?
            ros::param::get(common_string +  "/min_linear_speed", min_linear_speed) :
            min_linear_speed = 0.05;

    ros::param::has(common_string +  "/max_linear_speed") ?
            ros::param::get(common_string +  "/max_linear_speed", max_linear_speed) :
            max_linear_speed = 1.0;

    ros::param::has(common_string +  "/min_angular_speed") ?
            ros::param::get(common_string +  "/min_angular_speed", min_angular_speed) :
            min_angular_speed = 0.01;

    ros::param::has(common_string +  "/max_angular_speed") ?
            ros::param::get(common_string +  "/max_angular_speed", max_angular_speed) :
            max_angular_speed = 0.5;


    PID wheel_coefficient_linear(kpp, kip, kdp, min_linear_speed, max_linear_speed);
    PID wheel_coefficient_angular(kpp, kip, kdp, min_angular_speed, max_angular_speed);
    pid_cmd_vel_linear = wheel_coefficient_linear;
    pid_cmd_vel_angular = wheel_coefficient_angular;

    PID wheel_coefficient2_linear(kpg_linear, kig_linear, kdg_linear, min_linear_speed, max_linear_speed);
    PID wheel_coefficient2_angular(kpg_angular, kig_angular, kdg_angular, min_angular_speed, max_angular_speed);
    pid_distance = wheel_coefficient2_linear;
    pid_orientation = wheel_coefficient2_angular;

    int n;
    double wheel_radius, reduction_factor;

    ros::param::has(common_string + "/wheels/radius") ?
                ros::param::get(common_string + "/wheels/radius", wheel_radius) :
                wheel_radius = 0.0726;

    ros::param::has(common_string + "/wheels/reduction_factor") ?
                ros::param::get(common_string + "/wheels/reduction_factor", reduction_factor):
                reduction_factor = 9.0;

    ros::param::get(common_string + "/wheels/number", n);

    XmlRpc::XmlRpcValue v;

    nh_.param(common_string + "/wheels/coordinates", v, v);

    for(int i = 0; i < v.size(); ++i)
    {
          Wheel w{wheel_radius, {v[i][0], v[i][1], 0.0}, reduction_factor};
        wheels.push_back(w);
        wheels_speeds.push_back(0.);
    }

    ros::param::get(common_string + "/omnidirectional", omnidirectional);
    ros::param::get(common_string + "/correction_coefficient", correction_coefficient);

    ros::param::get(common_string + "/timeout", timeout_duration);


}


