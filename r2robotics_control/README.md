# R2Robotics Control package
**DISCLAIMER**: this package is still in progress and so is the documentation. Some parts not implemented yet are documented and some parts implemented are not documented yet, be patient, it's coming :)

## What is it ?
This package aims to define the control law for the R2Robotics robots! 

### What do you need ?
To do so, some packages are needed to _compile_ this node:
- roscpp
- r2robotics_msgs (Velocity and Movement messages needed)
- r2rocotics_srvs (setControlMaxLinearSpeed, setControlMaxAngularSpeed, stopControl, resumeControl, startRecalibration, stopRecalibration)
- std_msgs
- geometry_msgs
- r2robotics\_PR_base

To _run_ the package : rosrun r2robotics\_control r2robotics\_control_node

To _launch_ the package with roslaunch : roslaunch r2robotics_control control.launch 

### Available features
The control package has some features such as:
- **blocking detection** for mutiple scenarii: for a straight line it detects if the yaw has not been corrected for a long time, for an on place rotation if the robot moved to much (i.e. it can collide a border and move instead of rotating), and if the robot is just blocked by sometihng (border or other robot (the latest is not supposed to occur though)). 
- **timeout detection**: if the robot has not reached its position in 10s. a timeout is sent. 
- **changing max speed saturation dynamically**: via a service, see the dedicated section for more details.


### Parameters
Different parameters are available to run the control node:
- **Kp**, **Ki**, **Kd**: gains for the control node. They have different effects: 
	- **Kp** determines the convergence, however, it has an offset with the expected value. 
	- **Ki** determines the offset of the end value and the speed of convergence (however increasing Ki makes the robot oscillate!)
	- **Kd** determines the stability and smoothness of the control, increasing Kd would result in a softer convergence, but might make the control unstable. 
To tune those, you need to respect the namign convention: _control\_type_/_movement\_type/\_gain_. You can refer to the launch files for examples.
- **max speed** or **min speed**: all maximum and minimum speeds are tunable, as long as they have the right names. In order to tune those speeds, you must respect this naming conventio, (you can also refer to the launch files for examples): /_max\_linear\_speed_ or \_max\_angular\_speed_. Same with minimum speeds.
- **wheels**: cf. Configuration files part for more details.	
- **omnidirectional**: cf. Configuration files part for more details.
- **correction_coefficient**: cf. Configuration files part for more details.
- **timeout**: the duration in seconds the robot has to reach its goal before advertising a topic.

When you launch the node, you'll see some [INFO] giving you a summary of the loaded parameters. You can check your parameters were loaded correctly.

### Configuration files
A YAML file per robot is available in the cfg folder. It contains the coordinates of each wheel, its radius and other parameters that might be helpful to the robot, such as if we should consider a differential or omnidirectional robot. These parameters can also be set as options in the launch file. The parameters listed today in those files are:
- **wheels**
	- **number**: number of wheels the robot has 
	- **radius**: radius of the wheels
	- **reduction_factor**: reduction factor of the wheels 
	- **coordinates**: this is an array of the coordinates of the wheels, presented as [[x<sub>0</sub>, y<sub>0</sub>], [x<sub>1</sub>, y<sub>1</sub>],...]
- **omnidirectional**: boolean stating whether the robot is omnidirectional or differential
- **correction_coefficient**: coefficient for correcting the angle while the robot is moving for a differential robot. The coefficient is computed with a cosinus of the yaw error (yaw of the robot compared to the desired yaw). This error is multiplied by the _correction\_coefficient_, and we take the cosinus of this result to deccelerate the wheel making the robot drift.
	
So far there are two configuration files for the robots: _main\_badass\_robot.yaml_ and _cute\_little\_robot.yaml_

### Services available
Some services are available for trajectory and strategy needs:
- **set\_max\_linear\_speed** and **set\_max\_angular\_speed**: they speak for themselves, it allows to set the maximum speed while the node is running.
- **set\_min\_linear\_speed** and **set\_min\_angular\_speed**: they speak for themselves, it allows to set the minimum speed while the node is running.
- **stop\_control** and **resume\_control**: these services allow to stop the control algorithm and to resume it (resume last command or not). That is mainly useful for avoidance reasons. 
- **start\_recalibration** and **stop\_recalibration**: these services allow to start a recalibration by giving a vector the robot must follow until it is blocked by a wall, and to stop the recalibration when the trajectory or strategy thinks it is okay. The **start\_recalibration** service returns a bool _done_ when the recalibration started.
- **stop\_recalibration**: stops the recalibration, quite similar to **stop\_control**.
- **set\_control\_timeout**: sets the control timeout.

### Services called
The node calls some services:
- **waypoint_reached**: to notify the trajectory that we reached the goal 
- **moveTurretsRobotFrame**: to steer the turrets of the robot
- **setSelfRotation**: to steer the turrets in an on place rotation mode 


### Publishers and subscribers
The control node publishes on and subscribes to many topics! 

For the publications: 
- **cmd\_vel**: the node publishes on this topic when the simulation is running, it sends a velocity command to the simulated robot in the robot frame
- **motors\_speed\_request**: this is a topic where the wheels commands are sent
- **blocking\_detected**: this is the topic used to notify the other nodes the control has detected a blocking for some time
- **control\_timeout**: this is topic advertised when the robot has not reached its position after the timeout duration.

For the subscriptions:
- **gazebo/link\_states**: this is the equivalent of the odometry for the simulation of the robot with Gazebo
- **odometry\_feedback**: topic where the odometry feedback is published 
- **velocity\_control**: topic where the potential control (i.e. a velocity is sent) is published
- **global\_control**: topic where the global control (i.e. a mode is set, a position to reach or a yaw to reach...) is published

## How does it work? 
In this package two different types of control laws are implemented. Indeed, for strategy reasons, we need two different ways to control the robot: 
- a global one: the position to reach is sent to the control node, with the expected type of movement, and if needed the center of instant rotation. 
- a potential one: the expected velocities are sent to the robot (_x_, _y_ and _yaw_), which defines the type of movement. The velocities for each wheel are computed and sent. 

There is also a "common" control library implemented that is completely independent from any ROS libraries and set the bases of the control law. 

### The common library

### The control modes 
Here we detail all the different modes featured in the control node. 

#### Global mode 
The control node receives a **Movement** message. It contains:
- the type of movement: _arc circle_, _straight_ or _on place rotation_
- the position to reach: [x,y]
- the rotation center: [x<sub>R</sub>, y<sub>R</sub>] 
- the final yaw to reach 

The robot then computes, according to the required type of movement, its instruction and command. It first sets the steering of the wheels and wait 500ms when it receives an instruction, in order to avoid the robot to move when it is not ready to. Then the control will reach the wanted position. When this is done (i.e. the computed command is small enough for a certain duration), it sends a message to the **waypoint_reached** service.

#### Potential mode 



#### Recalibration mode 
In this mode, the control is deactivated, i.e. no real command is computed. The same command is sent to all the wheels in a certain direction. This is used to recalibrate the position of the robot so that the strategy and trajectory know where the robot is. It is to compensate the drift of the odometry.

### The control laws 
They are just simple PID controllers. The gains were tuned by hand and are accessible. However, in some cases, the derivative term is useless. 

There are default values set in the code if the gains are not set in the launch file or somewhere else. However, those gains are tuned to more or less work with the simulator and not at all in real life.